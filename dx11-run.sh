#!/bin/bash

trap "cleanup" EXIT

DOCKER_USER="user"
DOCKER_HOME="${1:-"$HOME/.dx11/home"}"
DOCKER_SHARE="$HOME/.dx11/share"
MOUNT_HOME="$HOME/.dx11/mount"

IMAGE="${IMAGE:-dx11-devbox}"
CAPS="--cap-add=SYS_MODULE --cap-add=SYS_RAWIO --cap-add=SYS_PACCT --cap-add=SYS_ADMIN --cap-add=SYS_NICE --cap-add=SYS_RESOURCE --cap-add=SYS_TIME --cap-add=AUDIT_CONTROL --cap-add=SYSLOG --cap-add=DAC_READ_SEARCH --cap-add=LINUX_IMMUTABLE --cap-add=IPC_LOCK --cap-add=IPC_OWNER --cap-add=SYS_PTRACE --cap-add=LEASE --cap-add=AUDIT_READ"
DISPLAY_NUM=3
TOR="${TOR:-no}"
#ADB="${ADB:-yes}"
ROOT="${ROOT:-no}"
ENCRYPT="${ENCRYPT:-yes}"
SYSTEMD="${SYSTEMD:-yes}"

HOST_IP=$(ip -4 addr show scope global dev docker0 | grep inet | awk '{print $2}' | cut -d / -f 1)
HOST_DOMAIN=host.local

# HELP

[ "$1" == "-h" -o "$1" == "--help" ] && {
	echo "$0 [persistent-home-folder]"
	echo "Allowed ENV vars: ENCRYPT (yes/no), SYSTEMD (yes/no) TOR (yes/no) and IMAGE"
	echo "example: IMAGE=x11docker/lxqt $0 $PWD/dockerHome"
	exit
}

# FOLDERS

[ ! -d "$DOCKER_HOME" ] && sudo -u $DOCKER_USER mkdir -p "$DOCKER_HOME"
[ ! -d "$DOCKER_SHARE" ] && sudo -u $DOCKER_USER mkdir -p "$DOCKER_SHARE"
[ ! -d "$MOUNT_HOME" ] && sudo -u $DOCKER_USER mkdir -p "$MOUNT_HOME"

# DUMMY ACTION TO REQUEST THE SUDO PASSWORD NOW

sudo docker ps > /dev/null 2>&1

# NETWORK

[ "$TOR" == "yes" ] && {
  sudo docker container ls | grep tor-router || {
	  sudo docker run -d --rm --name tor-router --cap-add NET_ADMIN --dns 127.0.0.1 flungo/tor-router || exit
  }
  TORARGS="--net=container:tor-router"
}

# GLOBAL SETTINGS

[ "$ROOT" == "yes" ] && {
  ROOTCMD="--sudouser"
}

[ "$SYSTEMD" == "yes" ] && {
  SYSTEMDCMD="--init=systemd --dbus=system"
}

[ "$ENCRYPT" == "yes" ] && {
  which ecryptfs-simple || { echo "please, install ecryptfs-simple"; exit; }
  mount | grep "^$DOCKER_HOME" || {
    sudo -u $DOCKER_USER ecryptfs-simple -a -c $HOME/.ecryptfs-config $DOCKER_HOME $MOUNT_HOME || { 
      echo "Unable to mount the encrypted folder" 
      exit
    }
  }
} || {
  MOUNT_HOME="$DOCKER_HOME"
  echo $MOUNT_HOME | grep "^/" || MOUNT_HOME="$PWD/$DOCKER_HOME"
}

# [ "$ADB" == "yes" ] && {
#   [ "$TOR" == "yes" ] && echo -e "WARNING: The TOR network is enabled. ADB connectivity will not work unless you disable it.\\n"
  
#   echo "Starting ADB server on host to listen on all interfaces"
#   adb -a nodaemon server &
#   ADB_SERVER_PID=$!
# }

# LAUNCH

echo "Starting DX11..."
sudo x11docker --hostipc $SYSTEMDCMD --user=$DOCKER_USER --home=$MOUNT_HOME -x --gpu -c --pulseaudio \
  --share $DOCKER_SHARE --display $DISPLAY_NUM --vt $DISPLAY_NUM --cap-default \
  -- $CAPS $TORARGS --add-host $HOST_DOMAIN:$HOST_IP --volume-driver memfs -- $IMAGE

cleanup
 

# HELPERS

cleanup () {
  [ "$ENCRYPT" == "yes" ] && {
    echo "Mounting encrypted Home folder"
    sudo -u $DOCKER_USER ecryptfs-simple -u $DOCKER_HOME
  }

  echo "Cleaning $MOUNT_HOME"
  sudo -u $DOCKER_USER rm -rf $MOUNT_HOME

  # [ "$ADB" == "yes" -a "$ADB_SERVER_PID" != "" ] && {
  #   echo "Stopping ADB server"
  #   kill $ADB_SERVER_PID
  # }

  [ "$TOR" == "yes" ] && {
    echo "Disposing TOR"
    sudo docker container ls | grep flungo/tor-router | awk '{print $1}' | xargs sudo docker container stop
  }
}
