#!/bin/bash

IMAGE="${IMAGE:-dx11-devbox}"
ID=$(sudo docker ps | grep $IMAGE | cut -d' ' -f1)
COMMAND=${1:-bash}

[ -z "$ID" ] && {
    echo "DX11 is not running. Can't attach to it."
    exit
}

sudo docker exec -it $ID $COMMAND

# COMMIT CHANGES?
echo
read -p "Do you want to commit the current state into the Docker image? (y/N) " response

[ "$response" == "y" -o "$response" == "Y" -o "$response" == "yes" -o "$response" == "Yes" -o "$response" == "YES" ] && {
    sudo docker commit $ID $IMAGE
}

echo "exit"
