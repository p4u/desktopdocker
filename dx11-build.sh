#!/bin/bash

DOCKER_BASE_IMAGE=dx11-base
DOCKER_DEVBOX_IMAGE=dx11-devbox

echo "Building the base DX11 image"
sudo docker build -t $DOCKER_BASE_IMAGE -f Dockerfile .

echo "Building the Android dev DX11 image"
sudo docker build -t $DOCKER_DEVBOX_IMAGE -f Dockerfile-devbox .
