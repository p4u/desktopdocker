#!/bin/bash

# DEPENDENCIES

echo "Installing ecrypt-simple"

[ ! -x "$(which ecryptfs-simple)" ] && {
    RELEASE=$(cat /etc/*-release | grep DISTRIB_ID | cut -d= -f2)
    
    case "$RELEASE" in
    "Ubuntu")
        ls /etc/apt/sources.list.d/home:obs_mhogomchungu.list || {
            sudo sh -c "echo 'deb http://download.opensuse.org/repositories/home:/obs_mhogomchungu/xUbuntu_19.04/ /' > /etc/apt/sources.list.d/home:obs_mhogomchungu.list"
        }
        wget -nv https://download.opensuse.org/repositories/home:obs_mhogomchungu/xUbuntu_19.04/Release.key -O- | sudo apt-key add - 
        sudo apt update
        sudo apt install -y ecryptfs-simple
        ;;
    "Debian")
        echo 'deb http://download.opensuse.org/repositories/home:/obs_mhogomchungu/Debian_9.0/ /' | sudo tee /etc/apt/sources.list.d/home:obs_mhogomchungu.list
        wget -nv https://download.opensuse.org/repositories/home:obs_mhogomchungu/Debian_9.0/Release.key -O- | sudo apt-key add -
        sudo apt update
        sudo apt install -y ecryptfs-simple
        ;;
    "Fedora")
        sudo dnf config-manager --add-repo https://download.opensuse.org/repositories/home:obs_mhogomchungu/Fedora_30/home:obs_mhogomchungu.repo
        sudo dnf install ecryptfs-simple
        ;;
    "OpenSuse")
        sudo zypper addrepo https://download.opensuse.org/repositories/home:obs_mhogomchungu/openSUSE_Tumbleweed/home:obs_mhogomchungu.repo
        sudo zypper refresh
        sudo zypper install ecryptfs-simple
        ;;
    "Arch" | "Manjaro")
        sudo pacman -S ecryptfs-simple
        ;;
    *)
        echo "Unsupported Linux distribution"
        ;;
    esac
}

# BUILD BASE AND DEVELOPER IMAGES

bash ./dx11-build.sh
