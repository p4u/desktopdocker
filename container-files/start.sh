#!/bin/bash

# SYNC CONFIG

[ ! -f "$HOME/.bashrc" ] && cp /etc/skel/.bashrc $HOME || true
[ ! -f "$HOME/.profile" ] && cp /etc/skel/.profile $HOME || true
[ ! -f "$HOME/.vimrc" ] && cp /etc/skel/.vimrc $HOME || true
[ ! -d "$HOME/.config" ] && cp -a /etc/skel/.config $HOME || true

# LAUNCH DESKTOP

startdde

echo "See you soon"
