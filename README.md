# DX11

Run sandboxed desktop Linux sessions using Docker and X11.

### Features

+ Immutable dockerized X11 sessions
+ Independent desktops created on TTY 3 (CTRL+ALT+F3)
+ Persistent `/home/user` folder
+ Home folder encryption (ecryptfs)
+ Transparent TOR routing
+ Dockerfiles to customize the base system and a richer developer environment
+ Higher responsiveness and lower CPU usage when compared to a typical VM

### Requirements

+ Docker
+ [X11 Docker](https://github.com/mviereck/x11docker#shortest-way-for-first-installation)
+ [ecryptfs-simple](https://software.opensuse.org/download.html?project=home%3Aobs_mhogomchungu&package=ecryptfs-simple)

### Set up

Set the environment up (Ubuntu, Debian, Fedora, OpenSuse, Arch, Manjaro)
```
./dx11-init.sh
```

Otherwise, disable the home encryption and run:
```
./dx11-build.sh
```

### Usage

Run DX11 using the default home folder (`~/.docker-home`)
```
./dx11-run.sh
```

Run DX11 using a custom folder as the Home
```
./dx11-run.sh ~/my-custom-folder
```

#### Custom settings

- `ENCRYPT` => `yes` or `no`
- `SYSTEMD` => `yes` or `no`
- `TOR` => `yes` or `no`
- `IMAGE` (a custom docker image of your choice)

Check out the already available images by x11docker https://hub.docker.com/u/x11docker

#### Custom builds

- Edit `Dockerfile` to add core components to the base system.
- Edit `Dockerfile-devbox` to add extra dependencies that you want to use on the Development image.

Run `./dx11-build.sh` to update the Docker images:
- `dx11-base`
- `dx11-devbox`

#### Interactive changes

Run `./dx11-exec.sh` to get a root shell on the running container.

When you log out, you will be given the option to commit the changes you made to the Docker image.

### Example

```
IMAGE=dx11-base TOR=no ./dx11-run.sh ~/dx11-home
```

### Development

By default, the `devbox` image features support for Golang, NodeJS, Flutter, the Android SDK and Java 8.

#### Android debugging

For security reasons, the `/dev` filesystem is not directly exposed to the container. This means that USB debugging is not directly possible.

To remotely debug on an Android device, you'll need to enable it on your device first:
- Enable the [Developer Mode](https://www.androidexplained.com/enable-developer-mode/) if it is not enabled already
- Connect your phone to a USB port
- Check that the device appears on `adb devices`
- On the host computer, run `adb tcpip 5555`
- Disconnect the USB cable

This needs to be done the first time.

Now, to debug applications from within the container:
- Connect your device to the same Wifi network as the computer
- Note the phone's local IP address
- On the container, run `adb connect <phone-address>:5555`
- Run `adb devices` to check that the device is recognized
- Launch your debugger of choice.

ADB will now work with the device as if it was physically connected.

### Folders

- `~/.dx11/home` is the default source folder that gets mounted on `/home/user`
- `~/.dx11/share` allows you to share files between host and container, both on the same location.
- `~/.dx11/mount` contains the unencrypted data mounted on the guest (only when encryption is enabled)
- `etc-skel` contains template files that are copied to `/home/user` when the session is started.
- `container-files` contains the main script that is run when launching the X11 session

### FAQ

- If you just want a minimal base system, use `IMAGE=dx11-base` instead of the default `IMAGE=dx11-devbox`
- If you use TOR, you might be unable to remotely debug with an Android device.
- Root and user password's are disabled. If you lock the screen you will have to kill the session from elsewhere.
- Building docker images may take disk space overtime. Run `docker image prune` to get rid of older images that are no longer tagged
- The default keyboard layout is US. `.profile` runs `setxkbmap <lang>` by you when opening a terminal

<!--
### Work in progress

- https://stackoverflow.com/questions/29563183/connecting-to-a-usb-android-device-in-a-docker-container-via-adb
    - https://learningbysimpleway.blogspot.com/2018/02/how-to-connect-adb-devices-to-linux.html
    - https://www.coveros.com/running-android-tests-in-docker/
- https://stackoverflow.com/questions/24225647/docker-a-way-to-give-access-to-a-host-usb-or-serial-device
-->
