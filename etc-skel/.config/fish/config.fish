# ALIAS

function l
	ls -lhGF $argv
end
function clh
   echo yes | history clear > /dev/null
   echo > ~/.local/share/fish/fish_history
end

alias gst="git status"
alias gd="git diff"
alias gl="git pull"
alias gp="git push"
alias grv="git remote -v"
alias gcmsg="git commit -m"

# ENV VARS

# STARTUP

clh
setxkbmap es
