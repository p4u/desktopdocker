FROM x11docker/deepin
ENV DEBIAN_FRONTEND noninteractive

# CORE COMPONENTS
RUN apt update && apt -y dist-upgrade && \
    apt install -y gnupg vim curl wget git build-essential fish net-tools dnsutils iputils-ping iproute2 telnet zip unzip tmux && \
	apt -y purge oneko && apt -y autoremove --purge && apt -y clean

# ADDITIONAL APPS
RUN curl -s https://brave-browser-apt-release.s3.brave.com/brave-core.asc | apt-key --keyring /etc/apt/trusted.gpg.d/brave-browser-release.gpg add - && \
	echo "deb [arch=amd64] https://brave-browser-apt-release.s3.brave.com/ buster main" > /etc/apt/sources.list.d/brave-browser-release-buster.list && \
	curl -s https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > /root/microsoft.gpg && \
	install -o root -g root -m 644 /root/microsoft.gpg /etc/apt/trusted.gpg.d/ && rm /root/microsoft.gpg && \
	echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list && \
	apt update && apt install -y brave-browser code gnome-terminal file-roller htop keepass2 wire-desktop telegram-desktop && \
	apt -y autoremove --purge && apt -y clean

# ADD TEMPLATE FILES AND SETTINGS
ADD ./etc-skel /etc/skel

CMD ["startdde"]
